@echo off
rem A batch script to build -> deploy -> restart
rem build.bat --complete maven-profile-1 maven-profile-2 ...
rem build.bat --incremental maven-profile-1 maven-profile-2 ...
rem -- Laurence Geng

set profiles=%~1

goto loopProfiles

:loopProfiles
shift
if "%~1"=="" (
    goto build
) else (
    set profiles=%~1,%profiles%
    goto loopProfiles
)

:build
echo.
echo ***************************************************************************************
echo BUILD...
echo ***************************************************************************************
echo.

if "%profiles%"=="" (
    call mvn clean package -DskipTests=true
) else (
    call mvn clean package -DskipTests=true -P%profiles%
)

if "%errorlevel%"=="1" goto :buildfailed

if "%buildMode%"=="complete" (
   call target\classes\deploy.bat --clean
) else (
   call target\classes\deploy.bat
)

if "%errorlevel%"=="1" goto :buildfailed

goto buildsuccess

:buildsuccess
echo.
echo.
echo ***************************************************************************************
echo BUILD SUCCESS!!
echo ***************************************************************************************
goto end

:buildfailed
echo.
echo.
echo ***************************************************************************************
echo BUILD FAILED!!
echo ***************************************************************************************
goto end

:end
