from airflow.models.dag import DAG
from airflow.utils.dates import days_ago
import datetime
import pendulum
from airflow.sensors.external_task_sensor import ExternalTaskSensor


local_tz = pendulum.timezone("Asia/Shanghai")
from dagen.operators import SSHOperator


schedule_interval="0 4 * * *"

dag = DAG(
**{
'dag_id': "dag02",
'catchup': False,
'start_date': datetime.datetime(2021, month=10, day=13, tzinfo=local_tz),
'schedule_interval': schedule_interval,
'default_args':
    {
        'owner': 'Avris',
        'depends_on_past': False,
        'retries': 2,
        'email': ['guanghu@tesla.com'],
        'email_on_failure': True,
        'email_on_retry': False
    }
}
)

task = SSHOperator(
    dag=dag,
    **{
        'task_id': "dag02",
        'ssh_conn_id': 'eden_yarn_stg',
        'command':'echo dag02'
       }
)