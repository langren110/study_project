from airflow.models.dag import DAG
from airflow.utils.dates import days_ago
import datetime
import pendulum

local_tz = pendulum.timezone("Asia/Shanghai")
from dagen.operators import SSHOperator
from airflow.operators.bash_operator import BashOperator


schedule_interval="0 4 * * *"

dag = DAG(
**{
'dag_id': "test_shell_java",
'catchup': False,
'start_date': datetime.datetime(2021, month=10, day=1, tzinfo=local_tz),
'schedule_interval': None,
'is_paused_upon_creation': False,
'default_args':
    {
        'owner': 'Avris',
        'depends_on_past': False,
        'retries': 2,
        'email': ['guanghu@tesla.com'],
        'email_on_failure': True,
        'email_on_retry': False

    }
}
)

cmd='''#!bin/bash
     for host in  02 03 04 05 06 07 08 09 10
     do
     scp  /airflow_prod/repos/cdp/L*.py pvg03s1aflap$host.teslamotors.com:$PWD
     done'''

task2 = BashOperator(
task_id='task2',
bash_command='echo hello',
dag=dag)

task2
