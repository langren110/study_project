from airflow.models.dag import DAG
from airflow.utils.dates import days_ago
import datetime
import pendulum
from airflow.sensors.external_task_sensor import ExternalTaskSensor
from airflow.operators.bash_operator import BashOperator

local_tz = pendulum.timezone("Asia/Shanghai")
from dagen.operators import SSHOperator


schedule_interval="40 9 * * *"



main_dag = DAG(
**{
'dag_id': "main_dag",
'catchup': False,
'start_date': datetime.datetime(2021, month=10, day=13, tzinfo=local_tz),
'schedule_interval': schedule_interval,
'default_args':
    {
        'owner': 'Avris',
        'depends_on_past': False,
        'retries': 2,
        'email': ['guanghu@tesla.com'],
        'email_on_failure': True,
        'email_on_retry': False
    }
}
)
ssh_hook_task = BashOperator(
task_id='ssh_hook_task',
bash_command='echo hello',
dag=main_dag
)