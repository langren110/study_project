#!/bin/bash
export APP_HOME="$(cd "`dirname $(readlink -nf "$0")`"/..; pwd -P)"
dag_name=$1
scp_all_nodes ()
{
    hostlist=`cat ${APP_HOME}/bin/hosts | awk '{ print $1 }'`;
    path="${APP_HOME}/wfl/etl_dag/${dag_name}.py"
    for host in $hostlist;
    do
        echo "=============="$host"==============" >> ${APP_HOME}/wfl/log/run.log
        scp ${path} $host:/airflow_prod/repos/cdp/
    done
}

