#!/usr/bin/env bash

export APP_HOME="$(cd "`dirname $(readlink -nf "$0")`"/..; pwd -P)"

source "$APP_HOME/bin/constants.sh"
source "$APP_HOME/bin/util.sh"

JOB_NAME=""
JOB_ALIAS=""
JOB_TAGS="app=sdl"
SRC_ACTION=""
SRC_TABLE=""
ACTION=""
SQL=""
SQL_FILES=""
SQL_PARAMS=""
SINK_ACTION=""
SINK_TABLE=""
HUDI_RECORD_KEY_FIELD=""
HUDI_PRECOMBINE_FIELD=""
HUDI_PARTITION_PATH_FIELD=""

JOB_CLASS="com.github.sdl.GenericJob"
JOB_ARGS_DIR="$APP_HOME/log/job-args"
ADHOC_SQL_DIR="$APP_HOME/log/adhoc-sqls"

# ----------------------------------------------    Script Functions    ---------------------------------------------- #

resolveDependentJars() {
    dependencyJarsList=""
    for jar in $APP_HOME/lib/*.jar
    do
        dependencyJarsList="$jar,$dependencyJarsList"
    done
    echo "${dependencyJarsList%,}"
}

execSql()
{
    jobName="$1"
    sqlFile="$2"
    echo  "debug:sqlPath -----> "$sqlFile
     /opt/bigdata/spark/current/bin/spark-sql \
    --master yarn \
    --deploy-mode client \
    --deploy-mode client \
    --name "$JOB_NAME" \
    --driver-memory 10G \
    --executor-memory 6G \
    --num-executors 8 \
    --executor-cores  2 \
    --conf 'spark.serializer=org.apache.spark.serializer.KryoSerializer' \
    --conf 'spark.sql.extensions=org.apache.spark.sql.hudi.HoodieSparkSessionExtension' \
    --conf 'spark.hadoop.mapreduce.input.pathFilter.class=org.apache.hudi.hadoop.HoodieROTablePathFilter' \
    --conf spark.sql.legacy.timeParserPolicy=LEGACY \
    --conf spark.executor.memoryOverhead=1G  \
    --conf spark.sql.adaptive.join.enabled=true \
    --conf spark.sql.adaptive.skewJoin.enabled=true \
    --conf spark.sql.shuffle.partitions=800 \
    --conf spark.default.parallelism=800 \
    --conf spark.sql.adaptive.skewJoin.skewedPartitionThresholdInBytes=500M \
    --queue batch \
    $SQL_PARAMS \
    --jars "$(resolveDependentJars)" \
    -f "$sqlFile"
}

startJob() {
    getJobName

    # If $JOB_ALIAS set, append it to JOB_NAME
    preferredJobName=$([ ! "$JOB_ALIAS" == "" ] && echo $JOB_ALIAS || echo $JOB_NAME)
    printHeading "START JOB [ $preferredJobName ]"

    # make job args file...
    DATE="$(date +%F)"
    NANO_SEC="$(date +%s%N)"
    mkdir -p "$JOB_ARGS_DIR/$DATE"
    jobRunFile="$JOB_ARGS_DIR/$DATE/${JOB_NAME}_${NANO_SEC}.json"
    cat <<-EOF > "$jobRunFile"
    {
        "JobName": "$JOB_NAME",
        "Arguments": {
            "--src-action": "$SRC_ACTION",
            "--src-table": "$SRC_TABLE",
            "--action": "$ACTION",
            "--sql-files": "$SQL_FILES",
            "--sql-params": "$SQL_PARAMS",
            "--sink-action": "$SINK_ACTION",
            "--sink-table": "$SINK_TABLE",
            "--hudi-record-key-field": "$HUDI_RECORD_KEY_FIELD",
            "--hudi-precombine-field": "$HUDI_PRECOMBINE_FIELD",
            "--hudi-partition-path-field": "$HUDI_PARTITION_PATH_FIELD"
        },
        "MaxCapacity": $GLUE_MAX_CAPACITY
    }
EOF
    echo "Job Run Configs:"
    echo ""
    cat "$jobRunFile"
    echo ""

    execSql "$JOB_NAME" "$SQL_FILES"

#    /opt/apps/spark-3.0.1-bin-hadoop3.2/bin/spark-sql \
#    --master local \
#    -f "$SQL_FILES"  \
#    --jars /opt/apps/hive-3.1.2/lib/mysql-connector-java-5.1.39.jar --driver-class-path /opt/apps/hive-3.1.2/lib/mysql-connector-java-5.1.39.jar
}

# execSqlFiles actually is starting a sql-specific job
execSqlFiles() {
    JOB_NAME="$ADHOC_SQL_JOB"
    ACTION="sql"
    startJob
}

parseArgs() {
    if [ "$#" -eq 0 ]; then
        #printUsage
        exit 0
    fi

    optString="job-name:,alias:,tags:,src-action:,src-table:,action:,sql:,sql-files:,sql-params:,sink-action:,sink-table:,hudi-record-key-field:,\
    hudi-precombine-field:,hudi-partition-path-field:"

    # IMPORTANT!! -o option can not be omitted, even there are no any short options!
    # otherwise, parsing will go wrong!
    OPTS=$(getopt -o "" -l "$optString" -- "$@")
    exitCode=$?
    if [ $exitCode -ne 0 ]; then
        echo ""
        printUsage
        exit 1
    fi
    eval set -- "$OPTS"
    while true; do
        case "$1" in
            --job-name)
                JOB_NAME="${2}"
                shift 2
                ;;
            --alias)
                JOB_ALIAS="${2}"
                shift 2
                ;;
            --tags)
                JOB_TAGS="${JOB_TAGS},${2}"
                shift 2
                ;;
            --src-action)
                SRC_ACTION="${2}"
                shift 2
                ;;
            --src-table)
                SRC_TABLE="$2"
                shift 2
                ;;
            --action)
                ACTION="$2"
                shift 2
                ;;
            --sql)
                SQL="$2"
                shift 2
                ;;
            --sql-files)
                SQL_FILES="$2"
                shift 2
                ;;
            --sql-params)
                SQL_PARAMS="$2"
                shift 2
                ;;
            --sink-action)
                SINK_ACTION="$2"
                shift 2
                ;;
            --sink-table)
                SINK_TABLE="$2"
                shift 2
                ;;
            --hudi-record-key-field)
                HUDI_RECORD_KEY_FIELD="$2"
                shift 2
                ;;
            --hudi-precombine-field)
                HUDI_PRECOMBINE_FIELD="$2"
                shift 2
                ;;
            --hudi-partition-path-field)
                HUDI_PARTITION_PATH_FIELD="$2"
                shift 2
                ;;
            --) # No more arguments
                shift
                break
                ;;
            *)
                echo ""
                echo "Invalid option $1." >&2
                printUsage
                exit 1
                ;;
        esac
    done
    shift $((OPTIND-1))
}

getJobName(){
  array=(`echo $JOB_NAME | tr '_' ' '` )
  path=''
  path="${APP_HOME}/sql"
  i=0
  for var in ${array[@]}
  do
    #echo "$i"
    if [ $i == 2 ] ; then
      break;
    fi
    path="${path}/${var}"
    #echo $var
    i=$(( $i+1 ))
  done
  SQL_FILES="${path}/${JOB_NAME}.sql"
}

getDefaultPartitionDate(){

pt=`date -d "1 day ago" +"%Y%m%d"`
SQL_PARAMS="--hivevar pt=$pt "
}

printUsage() {
    echo ""
    printHeading "USAGE"
    echo ""
    echo "SYNOPSIS"
    echo ""
    echo "$0 [ACTION] [--OPTION1 VALUE1] [--OPTION2 VALUE2]..."
    echo ""
    echo "ACTIONS:"
    echo ""
    echo "create                                create a job"
    echo "delete                                delete a job"
    echo "start                                 start a job"
    echo "exec-sql                              execute an ad-hoc sql"
    echo "exec-sql-files                        execute ad-hoc sql from sql files"
    echo "help                                  print usage"
    echo ""
    echo "OPTIONS:"
    echo ""
    echo "--job-name                            job name to be operate"
    echo "--alias                               job alias"
    echo "--tags                                tags for job"
    echo "--src-action                          action to be applied on source"
    echo "--src-table                           source table to be operated"
    echo "--action [sql|class]                  core action for etl operation, now support 2 types: sql or class"
    echo "--sql                                 sql to be executed"
    echo "--sql-files                           sql files to be executed"
    echo "--sql-params                          sql params, present as key-value pair string, i.e. year=2020,month=01"
    echo "--sink-action                         action to be applied on sink"
    echo "--sink-table                          sink table to be operated"
    echo "--hudi-record-key-field               hudi record key field for hudi table"
    echo "--hudi-precombine-field               hudi precombine field for hudi table"
    echo "--hudi-partition-path-field           hudi partition path field for hudi table"
    echo ""
    echo "EXAMPLES:"
    echo ""
    echo "# create job [ ods_tlc_yellow_trip_insert ] with tags"
    echo "$0 create --job-name ods_tlc_yellow_trip_insert --tags layer=ods,domain=tlc,action=etl"
    echo ""
    echo "# delete job [ ods_tlc_yellow_trip_insert ]"
    echo "$0 delete --job-name ods_tlc_yellow_trip_insert"
    echo ""
    echo "# start job [ ods_geo_taxi_zone_insert ] with parameters"
    echo "$0 start --job-name ods_geo_taxi_zone_insert --src-action load-csv-as-view --src-table stg.geo_taxi_zone --action sql --sql-files s3://$APP_BUCKET/sql/ods/geo/ods_geo_taxi_zone_insert.sql --sql-params year=2020,month=01 --sink-action save-view-to-hudi --sink-table ods.geo_taxi_zone --hudi-record-key-field locationid --hudi-precombine-field ts --hudi-partition-path-field year,month"
    echo ""
    echo "# execute an ad-hoc sql to drop table stg.pub_tmp"
    echo "$0 exec-sql --sql 'drop table if exists stg.pub_tmp'"
    echo ""
    echo "# execute an ad-hoc sql file drop all tmp tables on stg layer"
    echo "$0 exec-sql --sql-files s3://$APP_BUCKET/sql/cmn/drop_stg_tmp_tables.sql"
    echo ""
}

# -----------------------------------------------    Script Entrance    ---------------------------------------------- #
#getDefaultPartitionDate

parseArgs "$@"
case $1 in
    (create)
        createJob
    ;;
    (delete)
        deleteJob
    ;;
    (start)
        startJob
    ;;
    (exec-sql)
        execSql
    ;;
    (exec-sql-files)
        execSqlFiles
    ;;
    (help)
        printUsage
    ;;
    (*)
        printUsage
    ;;
esac
