#!/bin/bash
export APP_HOME="$(cd "`dirname $(readlink -nf "$0")`"/..; pwd -P)"

set -ex
echo `date` >> ${APP_HOME}/wfl/log/run.log
echo 'copy etl_dag.py start....' >> "${APP_HOME}/wfl/log/run.log"
#label_name and unique
dag_id=$1
# run_type unused
#run_type=$2

#interval 每天凌晨1点对前一天
schedule_interval='0 1 * * *'

#拷贝模板生成 code.py

echo "dag_id=${dag_id}" >> ${APP_HOME}/wfl/log/run.log
echo "schedule_interval=${schedule_interval}"  >> ${APP_HOME}/wfl/log/run.log

cp ${APP_HOME}/wfl/template/dag_template.py  ${APP_HOME}/wfl/etl_dag/${dag_id}.py
#修改code.py中的参数
sed -i s/@CODE/${dag_id}/g ${APP_HOME}/wfl/etl_dag/${dag_id}.py
sed -i s/@DAG_ID/${dag_id}/g ${APP_HOME}/wfl/etl_dag/${dag_id}.py
sed -i s/@schedule_interval/"${schedule_interval}"/g ${APP_HOME}/wfl/etl_dag/${dag_id}.py
echo "create finish!"  >> ${APP_HOME}/wfl/log/run.log
#copy code.py到airflow目录
#cp ${dag_id}.py
echo "start copy *.py to Airflow"  >> ${APP_HOME}/wfl/log/run.log
sh /home/airflow_template/remotecopy.sh  /home/airflow_template/dag_py/${dag_id}.py /airflow_prod/repos/cdp
echo "end copy"  >> ${APP_HOME}/wfl/log/run.log
echo `date`  >> ${APP_HOME}/wfl/log/run.log