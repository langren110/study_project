@echo off

set host=${deploy.host}
set user=${deploy.user.name}
set key=${deploy.user.key}
set baseDir=${deploy.user.home}
set appHome=${deploy.home}
set buildDir=${project.build.directory}
set binZip=${project.build.finalName}.zip

if "%~1"=="--clean" (
    PLINK -l %user% -i %key% %host% -t "if [ -d '%appHome%' ];then rm -rf %appHome%;fi"
)

echo.
echo ***************************************************************************************
echo UPLOAD...
echo ***************************************************************************************

@echo on
PSCP -l %user% -i %key% "%buildDir%\\%binZip%" "%host%:/tmp/"
PLINK -l %user% -i %key% %host% -t "if [ ! -d '%baseDir%' ];then mkdir %baseDir%;fi"
PLINK -l %user% -i %key% %host% -t "unzip -o /tmp/%binZip% -d %baseDir%/"
@echo off

echo.
echo ***************************************************************************************
echo STARTUP...
echo ***************************************************************************************

@echo on
PLINK -l %user% -i %key% %host% -t "%baseDir%/${project.build.finalName}/bin/sdl.sh deploy"
:: PLINK -l %user% -i %key% %host% -t "%baseDir%/${project.build.finalName}/bin/sdl.sh start-job sdl_dwh_geo_taxi_zone_etl from=20210401000000"
@echo off