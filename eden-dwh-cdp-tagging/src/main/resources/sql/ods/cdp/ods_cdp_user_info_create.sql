create table ods_cdp.user_info2 (
    id    int,
    name  string,
    price double
) using hudi
options (
type = 'cow',
primaryKey = 'id'
)
-- create table h3 using hudi
-- as
-- select 1 as id, 'a1' as name, 10 as price;