SET NAMES utf8mb4;

INSERT INTO `rule_execute_plan` (`id`, `type`, `name`, `code`, `sql1`, `sql2`, `relation`, `business_id`) VALUES
(42,	1,	'进店次数为1~3次',	'L19',	'select * from ads_cdp.user_baseinfo  where 1=1 and arrival_store_cnt >=  1  and arrival_store_cnt <=  3 ',	NULL,	1,	47),
(43,	1,	'最近一次进店距今时长在180天内',	'L20',	'select * from ads_cdp.user_baseinfo  where 1=1 and latestvisit <=  180 ',	NULL,	1,	48),
(44,	1,	'最近一次试驾车型为Model 3',	'L21',	'select * from ads_cdp.user_baseinfo  where 1=1 and lastesttdvtype =  \'Model 3\'',	NULL,	1,	49),
(45,	1,	'意向车型为Model3',	'L22',	'select * from ads_cdp.user_baseinfo  where 1=1 and productsinterested =  \'Model 3\'',	NULL,	1,	50),
(46,	1,	'credit重度用户',	'L23',	'select * from ads_cdp.user_baseinfo  where 1=1 and is_heavy_user <=  15 ',	NULL,	1,	51),
(47,	1,	'近30天内超充频次大于5次',	'L24',	'select * from ads_cdp.user_baseinfo  where 1=1 and total_sess_flag_last30 >  5 ',	NULL,	1,	52),
(48,	1,	'充电常驻城市为上海',	'L25',	'select * from ads_cdp.user_baseinfo  where 1=1 and often_charge_city =  \'Shanghai\'',	NULL,	1,	53),
(49,	1,	'所在省份上海',	'L26',	'select * from ads_cdp.user_baseinfo  where 1=1 and province =  \'上海\'',	NULL,	1,	54),
(50,	1,	'所在城市杭州市',	'L27',	'select * from ads_cdp.user_baseinfo  where 1=1 and city =  \'杭州市\'',	NULL,	1,	55),
(51,	1,	'最近一次购车车型为m3',	'L28',	'select * from ads_cdp.user_baseinfo  where 1=1 and model =  \'m3\'',	NULL,	1,	56),
(53,	1,	'最近一次购车金额大于30万',	'L30',	'select * from ads_cdp.user_baseinfo  where 1=1 and latest_vehicle_price >  300000 ',	NULL,	1,	58),
(54,	1,	'5月后购买车辆',	'L31',	'select * from ads_cdp.user_baseinfo  where 1=1 and order_placed_date >  \'2021-05-01 00:00:00\'',	NULL,	1,	59),
(55,	1,	'购车方式为全款',	'L32',	'select * from ads_cdp.user_baseinfo  where 1=1 and  order_type=\'CASH\'',	NULL,	1,	60);
