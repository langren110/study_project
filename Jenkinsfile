@Library('china-it-app-shared-library') _
def docker_registry = "china-biteam-docker-local.arf.tesla.cn"
def docker_img_name = "bi-eden-dwh"
def git_repo_name = "eden-dwh"
def docker_img = "${docker_registry}/${docker_img_name}"
// remote target path will be ${app_home}/${git_repo_name}/${branch} by default
def app_home = "/mnt/app"
pipeline {
  agent { node { label 'linux' } }
  options {
    timeout(time: 1, unit: 'HOURS')
    timestamps()
  }
  stages {
    stage('DEV: Pull, build and deploy') {
      when {
          branch 'develop'
      }
      steps {
        script {
          def branch = "develop"
          def maven_profile = "dev,core-bin"
          def airflow_server_list = "pvg03s1aflap01.teslamotors.com,pvg03s1aflap02.teslamotors.com,pvg03s1aflap03.teslamotors.com,pvg03s1aflap04.teslamotors.com,pvg03s1aflap05.teslamotors.com,pvg03s1aflap06.teslamotors.com,pvg03s1aflap07.teslamotors.com,pvg03s1aflap08.teslamotors.com,pvg03s1aflap09.teslamotors.com,pvg03s1aflap10.teslamotors.com"
          def remote_worker_node_host = "pvg03s1bieap007.cb1.pvg03.tzla.net"
          def commit_id = sh(returnStdout: true, script: 'git rev-parse HEAD').trim()
          println("commit_id: ${commit_id}")
          docker.withRegistry("https://${docker_registry}", 'sa-china-app-jenkins-arf') {
            withCredentials([file(credentialsId: 'jenkins-cn', variable: 'KUBECONFIG',)]) {
              sh """
                docker pull ${docker_img}
                docker run -u 0 --rm --env BRANCH=${branch} \
                --env APP_HOME=${app_home} \
                --env REMOTE_WORKER_NODE_HOST=${remote_worker_node_host} \
                --env MAVEN_PROFILE=${maven_profile} \
                --env GIT_REPO=${git_repo_name} \
                --env COMMIT_ID=${commit_id} \
                --env AIRFLOW_SERVER_LIST=${airflow_server_list} \
                --privileged \
                ${docker_img}
              """
            }
          }
        }
      }
    }
    stage('PRD: Pull, build and deploy') {
      when {
          branch 'master'
      }
      steps {
        script {
          def branch = "master"
          // TODO Replace below worker node with hostname in PRD once move to PRD
          def remote_worker_node_host = "pvg03s1bieap007.cb1.pvg03.tzla.net"
          def airflow_server_list = "pvg03s1aflap01.teslamotors.com,pvg03s1aflap02.teslamotors.com,pvg03s1aflap03.teslamotors.com,pvg03s1aflap04.teslamotors.com,pvg03s1aflap05.teslamotors.com,pvg03s1aflap06.teslamotors.com,pvg03s1aflap07.teslamotors.com,pvg03s1aflap08.teslamotors.com,pvg03s1aflap09.teslamotors.com,pvg03s1aflap10.teslamotors.com"
          // def remote_worker_node_host = "pvg03p1cdhap07.teslamotors.com"  // PRD
          def maven_profile = "prd,bin"
          def commit_id = sh(returnStdout: true, script: 'git rev-parse HEAD').trim()
          println("commit_id: ${commit_id}")
          docker.withRegistry("https://${docker_registry}", 'sa-china-app-jenkins-arf') {
            withCredentials([file(credentialsId: 'jenkins-cn', variable: 'KUBECONFIG',)]) {
              sh """
                docker pull ${docker_img}
                docker run -u 0 --rm --env BRANCH=${branch} \
                --env APP_HOME=${app_home} \
                --env REMOTE_WORKER_NODE_HOST=${remote_worker_node_host} \
                --env MAVEN_PROFILE=${maven_profile} \
                --env GIT_REPO=${git_repo_name} \
                --env COMMIT_ID=${commit_id} \
                --env AIRFLOW_SERVER_LIST=${airflow_server_list} \
                --privileged \
                ${docker_img}
              """
            }
          }
        }
      }
    }
  }
  post {
    always {
          sendNotifications(currentBuild.result,"GF3-BI-Jenkins-Notification")
    }
  }
}