FROM openjdk:8-jdk

ARG MAVEN_VERSION=3.8.3
ARG USER_HOME_DIR="/root"
ARG SHA=1c12a5df43421795054874fd54bb8b37d242949133b5bf6052a063a13a93f13a20e6e9dae2b3d85b9c7034ec977bbc2b6e7f66832182b9c863711d78bfe60faa
ARG BASE_URL=https://apache.osuosl.org/maven/maven-3/${MAVEN_VERSION}/binaries

RUN mkdir -p /usr/share/maven /usr/share/maven/ref \
  && curl -fsSL -o /tmp/apache-maven.tar.gz ${BASE_URL}/apache-maven-${MAVEN_VERSION}-bin.tar.gz \
  && echo "${SHA}  /tmp/apache-maven.tar.gz" | sha512sum -c - \
  && tar -xzf /tmp/apache-maven.tar.gz -C /usr/share/maven --strip-components=1 \
  && rm -f /tmp/apache-maven.tar.gz \
  && ln -s /usr/share/maven/bin/mvn /usr/bin/mvn


ENV MAVEN_HOME /usr/share/maven
ENV MAVEN_CONFIG "$USER_HOME_DIR/.m2"
ENV BRANCH master
ENV STASH_GIT_HOST "github.tesla.cn"
ENV ORG "BI"
ENV GIT_REPO eden-dwh
ENV APP_HOME /mnt/app
ENV CDP_TAG_MODULE_NAME eden-dwh-cdp-tagging
ENV CDP_ONEID_MODULE_NAME eden-dwh-cdp-oneid
ENV CDP_PARENT_MODULE_NAME eden-dwh-parent
ENV APP_PATH "$APP_HOME/$GIT_REPO"
ENV CDP_APP_TAG_PATH "$APP_PATH/$CDP_TAG_MODULE_NAME"
ENV CDP_APP_ONEID_PATH "$APP_PATH/$CDP_ONEID_MODULE_NAME"
ENV CDP_APP_PARENT_TAG_PATH "$APP_PATH/$CDP_PARENT_MODULE_NAME"
# REMOTE_WORKER_NODE_HOST used for submiting Spark application job
ENV REMOTE_WORKER_NODE_HOST "pvg03s1bieap007.cb1.pvg03.tzla.net"
ENV AIRFLOW_SERVER_LIST ""
ENV REMOTE_WORKER_USER "hadoop"
ENV COMMIT_ID ""
ENV CDP_DAG_RELATIVE_PATH "wfl"
ENV CDP_MAX_DAG_DEPLOYMENT_ATTEMPT_COUNT 3
ENV CDP_MAX_DAG_DEPLOYMENT_ATTEMPT_SLEEP_IN_SECS 3
ENV MAVEN_PROFILE "prd,bin"
# Precondition
#   1. AIRFLOW_SERVER_USER MUST be created in each Airflow worker node and .ssh&authorized_keys is set properly
#   2. AIRFLOW_DAG_PATH MUST exist in each Airflow worker node and permission is set properly for AIRFLOW_SERVER_USER
ENV AIRFLOW_SERVER_USER "jenkins"
ENV AIRFLOW_DAG_PATH "/airflow_prod/repos/cdp"

COPY ./.ssh /root/.ssh
COPY . $APP_PATH
# User-defined maven settings.xml with local maven miror site
COPY mvn_settings.xml /usr/share/maven/conf/settings.xml

# Compile in image build stage to keep dependencies in docker image and reduce compiling/packaging time in each CI/CD
RUN chmod 600 /root/.ssh/* \
  && export GIT_SSH_COMMAND='ssh -o StrictHostKeyChecking=no' \
  && cd $APP_PATH \
  && git clone -b ${BRANCH} git@${STASH_GIT_HOST}:${ORG}/${GIT_REPO}.git \
  && echo "Finished pulling latest codes." \
  && cd $CDP_APP_PARENT_TAG_PATH \
  && export MAVEN_OPTS="-Xms512m -Xmx1024m -Xss10m" \
  && mvn -q clean package -DskipTests \
  && echo "Finished building and packaging."

# entrypoint with main CI/CD steps including pull, build and deploy
COPY entrypoint.sh /usr/local/bin/entrypoint.sh

ENTRYPOINT ["bash", "/usr/local/bin/entrypoint.sh"]