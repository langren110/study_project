# my_project
#### 项目介绍
**本项目是xxx-cdp的项目工程 + 笔记，以真实项目改编而成，仅供学习参考使用，请勿商业化**:exclamation: 
1. SQL部分:etl数仓  （已构建并使用）   :white_check_mark: 
2. py部分:airflow的配置文件 （已构建并使用） :white_check_mark:
3. 代码部分:hudi2ck、RFM模型计算、one_id算法等  (此部分代码暂时隐藏)  :o: 
4. 人生阶段规划/工作备忘录    :yum: *更新中...*
5. 知识图谱  :yum: *更新中...*

***

#### 项目架构
##### 调度系统
**Ariflow**:
1. 执行单元DAG(python文件init)
2. DAG组成部分---Task
3. schedule关键参数---`schedule_interval`,cron表达式
4. 依赖关系实现  
Task之间使用>>表示执行顺序,并行使用`[TASK1,TASK2] >> TASK3`  
DAG之间使用`ExternalTaskSensor`和`ExternalTaskMarker`实现触点依赖


##### 数仓分层
**Data warehouse**:  
1. ods: 数据贴源层
2. dwd: 根据实际业务需求建立的各主题的数据明细表层
3. dws: 根据实际业务需求建立的各主题的数据聚合表层
4. ads: 根据实际业务需求建立的各主题的报表层


##### 数据流转
**Data circulation**:  
埋点日志/各业务系统数据/外部数据源----->数仓----->clickhouse


##### 数据存储
**Data store**: 
1. 基于hadoop的底层
2. 上层构建以hive表/hudi表为基本的数仓
3. clickhouse为上层应用访问提供数据支撑

##### 任务执行
**Task execute**
1. etl任务全部使用Spark SQL方式提交计算
2. yarn作为资源调度系统，分配任务资源
3. airflow作为任务调度系统，定时执行任务
4. 数据任务一般都为T+1周期

***
#### 知识图谱

  ##### :coffee:   大数据
1. Hadoop
2. Spark(Spark Core、Spark SQL、Spark MLlib机器学习)
3. hive/hive sql
4. Kafka
5. presto、clickhouse
6. 调度系统 airflow、azkaban

#####  :coffee: 开发语言
1. java：还是国内使用最多
2. c/c++：运行效率高，参加考证、比赛时使用 [目前学习算法、刷算法、考研中需要用到]
3. python: 爬虫采集数据、数据分析、算法模型、脚本
4. C#: 以前做web的时候用到过，毕设可能要做个系统(以前写过demo，前后端刚好拿来复用)
 
##### :coffee:数据结构 (待更新)

##### :coffee:算法(待更新)

##### :coffee:计算机知识
 (备战考研过程中，专业408 会学习到计算机知识，届时进行更新)

##### :coffee: 数学知识（待更新）
 
##### :coffee: 机器学习、人工智能、推荐算法、数学、计算机、编程、数据
*目前仅计划准备阶段，暂未开启学习*  [ 收藏的小手就没有停过  :joy: ]
1. [从入门到入土](https://ailearning.apachecn.org/#/)[数学、python知识都有，这两个是前提]

2. [机器学习用到的书籍和视频](https://github.com/linxid/Machine_Learning_Study_Path)
3. [机器学习Python算法实践](https://github.com/lawlite19/MachineLearning_Python)
4. [NLP民工乐园](https://github.com/fighting41love/funNLP)(机器学习用到的资源包、数据集等，例如据集、句子相似度匹配算法集合、bert资源、文本生成&摘要相关工具、cocoNLP信息抽取工具、国内电话号码正则匹配、清华大学XLORE:中英文跨语言百科知识图谱、清华大学人工智能技术系列报告、自然语言生成、NLU太难了系列、自动对联数据等等等等)
5. python一百天  [github地址](https://github.com/jackfrued/Python-100-Days)  里面也有视频，指向b站。

*书籍推荐*
1. 《统计学习方法 李航》[对应Python代码实现](https://github.com/Dod-o/Statistical-Learning-Method_Code)
2. 《机器学习实战》Peter Harrington [官网代码]()
3. 《机器学习 西瓜书 周志华》[配套南瓜书](https://github.com/datawhalechina/pumpkin-book)
4. 《深度学习》
5. 《裂变》王天一 `豆瓣评分高，人工智能知识体系全` *极客里面有两节课可以结合着看，王天一的*


*网站/平台/编程*
1. Acwing编程学习平台 [c++学习、刷题/python练习/编程/数据结构/算法、考研数据结构](https://www.acwing.com/problem/)
2. AI Learning学习平台 [丰富的学习路线平台:python-->线性代数-->机器学习-->自然语言处理](https://apachecn.gitee.io/ailearning/#/)  [暂时无了]
3. 头歌学习平台 [丰富的计算机相关课程:计算机/数学/机器学习----亮点是学习+平台自带环境实践](https://www.educoder.net/paths)
4. 极客时间平台  [数据结构和算法、机器学习相关-----机器学习部分特别是王天一老师的](https://time.geekbang.org/column/intro/100003101?tab=comment)
5. 吴承恩科学家 [人工智能领学家、领头羊系列必看课程-----网易云平台](https://study.163.com/courses-search?keyword=%E5%90%B4%E6%81%A9%E8%BE%BE#/?scht=10)  b站也有中英双语
***
#### 规划-备忘录

    1. 目前：就职于上海一家数字科技公司，任职大数据工程师。  
	预计划在明年3月底之前完成c++、数据结构、算法部分，以及高数一部分的内容的学习。  
	之后将会备战考研，希望可以一战成硕！
  
    2. 未来规划：读研期间，继续专攻算法、机器学习路线。  
	大数据之路转向机器学习、人工智能、推荐算法系列！  
	但并不意味着大数据的历程就废弃掉了，相反大数据的经验是后面机器学习、算法模型的基石！  
	大数据这一侧是采集数据、储存数据、处理数据、分析数据。  
	但如何有效的分析，精确的分析，需要一个有用、高效的算法，而算法背后又是依靠着有效的数据这一端。
	但算法的体现是显而易见的，能为公司的效益提升产生卓有成效、显而易见的结果，当然，同时，机器学习、推荐算法岗的薪资也是很可观的，但是背后需要努力和坚持。
	奥利给！

# 学习记录
[传送门](https://gitee.com/zhoustarstar/study_project/wikis/%E5%AD%A6%E4%B9%A0%E8%AE%B0%E5%BD%95)
## 介绍
本部分按照知识图谱部分进行日常学习记录，会布置题目，会打卡.
### c/c++
***
#### 数组
- 2021.11.5记录:完成平方矩阵I、II、III
##### 753. 平方矩阵 I 
###### 题目描述
输入整数 N，输出一个 N 阶的回字形二维数组。

数组的最外层为 1，次外层为 2，以此类推。

###### 输入格式
输入包含多行，每行包含一个整数 N。

当输入行为 N=0 时，表示输入结束，且该行无需作任何处理。

###### 输出格式
对于每个输入整数 N，输出一个满足要求的 N 阶二维数组。

每个数组占 N 行，每行包含 N 个用空格隔开的整数。

每个数组输出完毕后，输出一个空行。

###### 数据范围
0≤N≤100
###### 输入样例：
```
1
2
3
4
5
0
```
###### 输出样例：
```
1

1 1
1 1

1 1 1
1 2 1
1 1 1

1 1 1 1
1 2 2 1
1 2 2 1
1 1 1 1

1 1 1 1 1
1 2 2 2 1
1 2 3 2 1
1 2 2 2 1
1 1 1 1 1
```
###### 解析

![输入图片说明](https://images.gitee.com/uploads/images/2021/1105/210620_458e1d2b_5089839.png "002.png")
1. *如图示围城一个回字形*
2. *通过观察发现每一个单元格元素显示数字为到各边界值的最小值*
3. *比如(1,5) 显示为1 它到上距离为i=1 右距离为n-j+1即5-5+1=1 左距离j=5 下距离n-i+1即5-1+1=5*
4. *比较四个值的最小值 用函数min嵌入min(min(a,b),min(a,b))即可，代码如下(`已通过AC`)*
```
#include <iostream>
using namespace std;

int main()
{

    int n;
    while(cin >> n && n>0)
    {
        for(int i=1;i<=n;i++)
        {
            for(int j=1;j<=n;j++)
            {
                int up=i;
                int left=j;
                int down=n-i+1;
                int right=n-j+1;
                cout << min(min(up,down),min(left,right)) << " " ;
            }
            cout << endl;
            
        }
        cout << endl;
    }
    return 0;
}


```
##### 754. 平方矩阵 II
###### 题目描述
输入整数 N，输出一个 N 阶的二维数组。

数组的形式参照样例。

###### 输入格式
输入包含多行，每行包含一个整数 N。

当输入行为 N=0 时，表示输入结束，且该行无需作任何处理。

###### 输出格式
对于每个输入整数 N，输出一个满足要求的 N 阶二维数组。

每个数组占 N 行，每行包含 N 个用空格隔开的整数。

每个数组输出完毕后，输出一个空行。

###### 数据范围
0≤N≤100
###### 输入样例：
```
1
2
3
4
5
0
```
###### 输出样例：
```
1

1 2
2 1

1 2 3
2 1 2
3 2 1

1 2 3 4
2 1 2 3
3 2 1 2
4 3 2 1

1 2 3 4 5
2 1 2 3 4
3 2 1 2 3
4 3 2 1 2
5 4 3 2 1
```
###### 解析
![输入图片说明](https://images.gitee.com/uploads/images/2021/1105/164220_87f2c83c_5089839.png "111.png")
1. *这是一道找规律的题，类似平方矩阵I，这类题大多通过找横纵坐标(i/j)的关系，得到规律*
2. *通过观察发现，对角线i、j相等之处，是其他的数是由1往两边发散*
3. *继续通过观察，判断(除i、j相等之处)的数，都是横纵坐标之差的绝对值+1得到，于是手撸代码如下*
```
#include <iostream>

using namespace std;
int main()
{
    int n;
    while(cin >> n && n>0)
    {
        for(int i=1;i<=n;i++)
        {
            for(int j=1;j<=n;j++)
            {
                if(i==j) cout << 1 << " ";
                else cout << abs(i-j)+1 << " ";
            }
            cout << endl;
        }
        cout << endl;
    }
    return 0;
}


```
###### 规律总结
1. *编程就是找规律，写逻辑，通过将数学问题的规律转化为编程逻辑实现*
2. *程序要优化,力争简洁、可读性强*
3. *从开始的y总讲的菱形，曼哈顿距离，到平方矩阵I到x轴、y轴距离，再到现在平方矩阵II的绝对值之差*
4. *此类问题就是找特殊点、特殊位置，观察特殊位置，寻找i、j之间的关系，寻找距离比例、和差积余等*
##### 755. 平方矩阵 III
###### 题目描述
输入整数 N，输出一个 N 阶的二维数组 M。

这个 N 阶二维数组满足 M[i][j]=2i+j。

具体形式可参考样例。

###### 输入格式
输入包含多行，每行包含一个整数 N。

当输入行为 N=0 时，表示输入结束，且该行无需作任何处理。

###### 输出格式
对于每个输入整数 N，输出一个满足要求的 N 阶二维数组。

每个数组占 N 行，每行包含 N 个用空格隔开的整数。

每个数组输出完毕后，输出一个空行。

###### 数据范围
0≤N≤15
###### 输入样例：
```
1
2
3
4
5
0
```
###### 输出样例：
```
1

1 2
2 4

1 2 4
2 4 8
4 8 16

1 2 4 8
2 4 8 16
4 8 16 32
8 16 32 64

1 2 4 8 16
2 4 8 16 32
4 8 16 32 64
8 16 32 64 128
16 32 64 128 256
```
*废话不多说，直接上代码，解析在代码里*
```
#include <iostream>
#include <cmath>

using namespace std;

int main()
{
    int n;
    int num;
    while(cin >> n && n>0)
    {
        for(int i=1;i<=n;i++)
        {
            for(int j=1;j<=n;j++)
            {
                //用到的时候百度的，c++里面求某个数的n次方
                // 使用pow函数 pow(底数，幂次)
                //pow函数 需要引用cmath库
                num=pow(2,i+j-2);
                
                //观察 i 、j 关系 可以得出 每个数是2的 i+j-2 次方
                cout << num << " ";
            }
            cout << endl;
        }
        cout << endl;
    }
    return 0;
}

```
#### 字符串
- 2021.11.28日记录  
[题目传送门](https://www.acwing.com/user/myspace/index/114892/)
1. c++ 基础编程打卡排名升至1500名以内，目前排名：1495名 [总人数6089]
2. 刷题10道
3. 字符串今日总结
```
- //===================字符串======================
- 1.获取一行字符串  string str; getline(cin,str); 包含空格
- 2.获取一行字符串  char c[]; fget(c,size,stdin); 包含空格
- 3.获取一个字符串  string a; cin >> a;遇到空格和回车终止
- //字符串的一些方法
- //转大小写 tolower(char c)  toupper(char c)
- //比较字符串大小 忽略大小写 
- // strcasecmp(const char a[] ,const char b[])
- //字符串转字符数组  xxx.c_str()
```